
//import edu.princeton.cs.introcs.In;
import java.util.Comparator;
import java.util.Iterator;

public class Board {

    private final int[][] board;
    private final int row;
    private final int column;
    private boolean firstAttempt = true;
    private int manhattan;

    public Board(int[][] blocks) {
        int N = blocks.length;
        board = new int[N][N];
        int x = 0, y = 0;
        for (int i = 0; i < N; i++) {       //goal
            for (int j = 0; j < N; j++) {
                board[i][j] = blocks[i][j];
                if (blocks[i][j] == 0) {
                    y = i;
                    x = j;
                }
            }
        }
        row = y;
        column = x;
    } // construct a board from an N-by-N array of blocks
    // (where blocks[i][j] = block in row i, column j)

    public int dimension() {
        return board.length;
    } // board size N

    public int hamming() {  //good
        int sum = 0;
        int N = board.length;
        int goal = 0;
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                goal++;
                if (board[i][j] != 0 && board[i][j] != goal && goal != N * N) {
                    sum++;
                }
            }
        }
        if (board[N - 1][N - 1] != 0) {
            sum++;
        }
        return sum;
    } // number of blocks out of place

    public int manhattan() { //good 
        if(!firstAttempt){
           return this.manhattan;
        }
        int sum = 0;
        int N = board.length;
        int goal = 0, goal1 = 0;
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                goal++;
                goal = goal == N * N ? 0 : goal;
                if (board[i][j] != 0 && board[i][j] != goal) {
                    goal1 = 0;
                    for (int k = 0; k < N; k++) {
                        for (int l = 0; l < N; l++) {
                            goal1++;
                            if (board[i][j] == goal1) {
                                sum += Math.max(i, k) - Math.min(i, k);
                                sum += Math.max(j, l) - Math.min(j, l);
                            }
                        }
                    }
                }
            }
        }
        this.manhattan = sum;
        this.firstAttempt = false;
        return sum;
    
    } // sum of Manhattan distances between blocks and goal

    public boolean isGoal() { //good?
        int N = board.length;
        int goal = 0;
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                goal++;
                if (board[i][j] != goal) {
                    if(i == N - 1 && j == N -1 ){
                        return board[i][j] == 0;
                    }
                    return false;
                }
            }
        }
       return true;
    } // is this board the goal board?

    public Board twin() { //good        
        int[][] temp = new int[this.dimension()][this.dimension()];
        for (int i = 0; i < this.dimension(); i++) {
            for (int j = 0; j < this.dimension(); j++) {
                temp[i][j] = this.board[i][j];
            }
        }
        if (temp[0][0] != 0 && temp[0][1] != 0) {
            int temp1 = temp[0][0];
            temp[0][0] = temp[0][1];
            temp[0][1] = temp1;
        } else {
            int temp1 = temp[1][0];
            temp[1][0] = temp[1][1];
            temp[1][1] = temp1;
        }
        Board b = new Board(temp);
        return b;
    }  // a board obtained by exchanging two adjacent blocks in the same row

    public boolean equals(Object y) {
        if (!(y instanceof Board)) {
            return false;
        }
        Board that = (Board) y;
        int thisD = this.dimension(), thatD = that.dimension();
        if (thisD != thatD) {
            return false;
        }
        int goal = 0;
        for (int i = 0; i < thisD; i++) {
            for (int j = 0; j < thatD; j++) {
                if (this.board[i][j] != that.board[i][j]) {
                    return false;
                }
            }
        }
        return true;
    } // does this board equal i0?

    public Iterable<Board> neighbors() {
        Queue<Board> q = new Queue<Board>();
        int i = row, j = column;
        if (valid(i - 1, j)) {
            q.enqueue(swap(i - 1, j, i, j));
        }
        if (valid(i, j + 1)) {
            q.enqueue(swap(i, j + 1, i, j));
        }
        if (valid(i + 1, j)) {
            q.enqueue(swap(i + 1, j, i, j));
        }
        if (valid(i, j - 1)) {
            q.enqueue(swap(i, j - 1, i, j));
        }
        return q;
    } // all neighboring boards

    private boolean valid(int i, int j) {
        return i > -1 && i < this.board.length && j > -1 && j < this.board.length;
    }

    private Board swap(int i, int j, int x, int y) {
        int N = this.dimension();
        int[][] temp = new int[N][N];
        for (int k = 0; k < N; k++) {
            for (int l = 0; l < N; l++) {
                temp[k][l] = board[k][l];
            }
        }
        exchange(i, j, x, y, temp);
        return new Board(temp);
    }

    private void exchange(int i, int j, int x, int y, int[][] temp) {
        int t = temp[x][y];
        temp[x][y] = temp[i][j];
        temp[i][j] = t;
    }

    public String toString() {
        int N = board.length;
        StringBuilder s = new StringBuilder();
        s.append(N + "\n");
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                s.append(String.format("%2d ", board[i][j]));
            }
            s.append("\n");
        }
        return s.toString();
    } // string representation of the board (in the output format specified below)

    public static void main(String[] args) {
        In in = new In("puzzle2x2-unsolvable1.txt");
        int N = in.readInt();
        int[][] blocks = new int[N][N];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                blocks[i][j] = in.readInt();
            }
        }
        Board b = new Board(blocks);
        System.out.println(b.toString());
        System.out.println(b.isGoal());
    } // unit test
}
