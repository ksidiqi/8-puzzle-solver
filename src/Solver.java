
import edu.princeton.cs.introcs.In;
import edu.princeton.cs.introcs.StdOut;
import java.util.Iterator;

public class Solver {

    private final MinPQ<SearchNode> mPQ = new MinPQ<SearchNode>();
    private final boolean solvable;
    private final int moves;
    private final Stack<Board> s = new Stack<Board>();

    private class SearchNode implements Comparable<SearchNode> {

        private final int moves;
        private final SearchNode previous;
        private final Board board;

        public SearchNode(Board c, SearchNode p, int i) {
            board = c;
            previous = p;
            moves = i;
        }

        public int compareTo(SearchNode that) {
            return (this.board.manhattan() + this.moves) - (that.board.manhattan() + that.moves);
        }
    }

    public Solver(Board initial) {
        SearchNode node = new SearchNode(initial, null, 0);
        mPQ.insert(node);
        //
        MinPQ<SearchNode> tmPQ = new MinPQ<SearchNode>();
        SearchNode tNode = new SearchNode(initial.twin(), null, 0);
        tmPQ.insert(tNode);
        //
        while (!mPQ.min().board.isGoal() && !tmPQ.min().board.isGoal()) {
            node = mPQ.delMin();
            for (Board neighbor : node.board.neighbors()) {
                if (doesNotHave(node, neighbor)) {
                    SearchNode temp = new SearchNode(neighbor, node, node.moves + 1);
                    mPQ.insert(temp);
                }
            }
            tNode = tmPQ.delMin();
            for (Board neighbor : tNode.board.neighbors()) {
                if (tNode.previous == null || !tNode.previous.board.equals(neighbor)) {
                    SearchNode temp = new SearchNode(neighbor, tNode, tNode.moves + 1);
                    tmPQ.insert(temp);
                }
            }
        }
        solvable = mPQ.min().board.isGoal();
        if (this.isSolvable()) {
            moves = mPQ.min().moves;
        } else {
            moves = -1;
        }
        SearchNode c = mPQ.delMin();
        while (c != null) {
            s.push(c.board);
            c = c.previous;
        }

    } // find a solution to the initial board (using the A* algorithm)

    private boolean doesNotHave(SearchNode sn, Board b) {
        Board searchBord = null;
        while (sn.previous != null) {
            searchBord = sn.previous.board;
            if (b.equals(searchBord)) {
                return false;
            }
            sn = sn.previous;
        }
        return true;
    }

    public boolean isSolvable() {
        return solvable;
    } // is the initial board solvable?

    public int moves() {
        return moves;
    } // min number of moves to solve initial board; -1 if no solution

    public Iterable<Board> solution() {
        if (!this.isSolvable()) {
            return null;
        }
        return s;
    } // sequence of boards in a shortest solution; null if no solution   

    public static void main(String[] args) {
        In in = new In("puzzle.txt");
        int N = in.readInt();
        int[][] blocks = new int[N][N];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                blocks[i][j] = in.readInt();
            }
        }
        Board initial = new Board(blocks);

        // solve the puzzle
        Solver solver = new Solver(initial);

        // print solution to standard output
        if (!solver.isSolvable()) {
            StdOut.println("No solution possible");
        } else {
            StdOut.println("Minimum number of moves = " + solver.moves());
            for (Board board : solver.solution()) {
                StdOut.println(board);
            }
        }
    } // solve a slider puzzle (given below)
}
